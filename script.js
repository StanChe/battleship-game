var randomLocation = Math.floor(Math.random() * 5) +1 ; // numbers 1-5, so 0 would not be generated
var shipLoc = randomLocation;
var shipLoc2 = shipLoc + 1;
var shipLoc3 = shipLoc2 + 1;
var shipLives = 3;
var moveHistory = [];
var moveNum=0

alert ("This is a battleship game that uses one line dimensions. The ship spans on 3 locations and is placed somewhere between 1-8. Find the ship using the prompt")

while (shipLives > 0) {
    var guess = prompt("Enter the coordinates to shoot");
    if (guess < 0 || guess > 8 || isNaN(guess)) {
        alert ("Pick a number from 1 to 8")
    }
    else if ((guess == shipLoc || guess == shipLoc2 || guess == shipLoc3) && (!moveHistory.includes(guess))) {
        moveHistory.push(guess)
        alert("You've hit the ship!")
        shipLives--
        moveNum++
    } 
   else if (moveHistory.includes(guess)) {
        alert("You have already shot in here, try another location")
    } else {
        moveHistory.push(guess)
        alert("You missed, try again")
        moveNum++
    }
}
alert(`Congratulations, you won! You've beat the game in ${moveNum} turns`)